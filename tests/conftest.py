import pytest
from django.contrib.auth.models import User
from django_webtest import DjangoTestApp


@pytest.fixture()
def user(django_app: DjangoTestApp) -> User:
    user = User.objects.create_user(username='user', password='secret')
    form = django_app.get('/sign/signin/').form
    form['username'] = 'user'
    form['password'] = 'secret'
    form.submit().follow()
    return user
