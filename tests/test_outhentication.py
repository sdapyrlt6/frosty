import pytest
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.test import Client


def test_login(client: Client):
    resp = client.get('/sign/signin/')
    # 200 ar atsidaro puslapis, 302 ar nukreipia
    assert resp.status_code == 200
    assert isinstance(resp.context['form'], AuthenticationForm)


@pytest.mark.django_db
def test_login_form(client: Client):
    User.objects.create_user(username='testas', password='slaptas')
    resp = client.post('/sign/signin/', {
        'username': 'testas',
        'password': 'slaptas',
    })
    assert resp.status_code == 302, resp.context['form'].errors.as_text()
    assert resp.headers['location'] == '/'





