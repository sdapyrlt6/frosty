import pytest
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.test import Client
from django_webtest import DjangoTestApp
from django_webtest import DjangoWebtestResponse

from frosty.forms import ProductForm
from frosty.forms import ProductItemForm
from frosty.models import Location, Product
from frosty.models import ProductItem
from frosty.models import Shop


@pytest.mark.django_db
def test_location_list_filtered_by_user(django_app: DjangoTestApp, user: User):
    user2 = User.objects.create_user(username='user2', password='testas2')
    l1 = Location.objects.create(name='L1', user=user)
    Location.objects.create(name='L2', user=user2)
    resp: HttpResponse = django_app.get('/')
    assert list(resp.context['locations']) == [l1]


@pytest.mark.django_db
def test_location_by_anonymous_user(django_app: DjangoTestApp):
    user1 = User.objects.create_user(username='user1', password='testas1')
    user2 = User.objects.create_user(username='user2', password='testas2')
    Location.objects.create(name='L1', user=user1)
    Location.objects.create(name='L2', user=user2)
    resp: DjangoWebtestResponse = django_app.get('/').follow()
    assert resp.request.path == '/sign/signin/'


@pytest.mark.django_db
def test_products_form(django_app: DjangoTestApp, user: User):
    location = Location.objects.create(name='spintele', user=user)
    resp: DjangoWebtestResponse = django_app.get(f'/products/{location.id}/new/')
    assert isinstance(resp.context['product_form'], ProductForm)
    assert isinstance(resp.context['product_item_form'], ProductItemForm)
    assert 'location' not in resp.form.fields


@pytest.mark.django_db
def test_products_form_submit(django_app: DjangoTestApp, user: User):
    location = Location.objects.create(
        name='Šaldytuvas',
        description='',
        user=user,
    )
    resp: DjangoWebtestResponse = django_app.get(f'/products/{location.id}/new/')
    resp.form['name'] = 'pienas'
    resp.form['type'] = 'milk'
    resp.form['amount'] = '2'
    resp.form['unit'] = 'l'
    resp.form['expiration_date'] = '2022-01-01'
    resp.form['shop_name'] = 'Lidl'

    resp = resp.form.submit().follow()
    assert resp.request.path == f'/products/{location.id}'
    shop_lidl = Shop.objects.get(name='Lidl')
    assert Product.objects.filter(name='pienas').exists()
    assert (
        ProductItem.objects.
        filter(
            product__name='pienas',
            amount=2,
            unit='l',
            shop=shop_lidl,
            location=location
        ).
        exists()
    )


@pytest.mark.django_db
def test_products_form_submit_with_exiting_shop(django_app: DjangoTestApp, user: User):
    shop = Shop.objects.create(name='Lidl')
    location = Location.objects.create(
        name='Šaldytuvas',
        description='',
        user=user,
    )
    resp: DjangoWebtestResponse = django_app.get(f'/products/{location.id}/new/')
    resp.form['name'] = 'pienas'
    resp.form['type'] = 'milk'
    resp.form['amount'] = '2'
    resp.form['unit'] = 'l'
    resp.form['expiration_date'] = '2022-01-01'
    resp.form['shop_name'] = ''
    resp.form['shop'] = shop.id

    resp = resp.form.submit().follow()
    assert resp.request.path == f'/products/{location.id}'
    assert Product.objects.filter(name='pienas').exists()
    assert (
        ProductItem.objects.
        filter(
            product__name='pienas',
            amount=2,
            unit='l',
            shop=shop,
            location=location
        ).
        exists()
    )


@pytest.mark.django_db
def test_products_form_submit_with_exiting_shop_add_new_shop(django_app: DjangoTestApp, user: User):
    shop = Shop.objects.create(name='Lidl')
    location = Location.objects.create(
        name='Šaldytuvas',
        description='',
        user=user,
    )
    resp: DjangoWebtestResponse = django_app.get(f'/products/{location.id}/new/')
    resp.form['name'] = 'pienas'
    resp.form['type'] = 'milk'
    resp.form['amount'] = '2'
    resp.form['unit'] = 'l'
    resp.form['expiration_date'] = '2022-01-01'
    resp.form['shop_name'] = 'Lidl'
    resp.form['shop'] = shop.id

    resp = resp.form.submit(status=200)
    assert resp.context['product_item_form'].errors.get_json_data() == {
        'shop_name': [{
            'message': 'taip negalima, nu nu nu!!! turi buti tik vienas parduotuve, arba pasirinkta, arna naujai irasyta',
            'code': ''
        }]
    }


@pytest.mark.django_db
def test_products_form_submit_error(client: Client):
    user = User.objects.create_user(username='user1', password='testas1')
    client.login(username='user1', password='testas1')
    location = Location.objects.create(
        name='Šaldytuvas',
        description='',
        user=user,
    )
    resp = client.post(f'/products/{location.id}/new/', {
        'name': '',
        'type': 'milk',
    })
    assert not Product.objects.filter(name='').exists()
    assert resp.status_code == 200
    assert resp.context['form'].errors.get_json_data() == {'name': [
        {'code': 'required', 'message': 'This field is required.'}
    ]}


@pytest.mark.django_db
def test_product_list_at_location(client: Client):
    user1 = User.objects.create_user(username='user1', password='testas1')
    l1 = Location.objects.create(name='L1', user=user1)
    product = Product.objects.create(name='pienas')
    product_item = ProductItem.objects.create(
        amount=2,
        product=product,
        location=l1,
    )
    client.login(username='user1', password='testas1')
    resp: HttpResponse = client.get(f'/products/{l1.id}')
    assert resp.status_code == 200
    products = [form.instance for form in resp.context['products']]
    assert products == [product_item]


@pytest.mark.django_db
def test_locations_form(client: Client):
    User.objects.create_user(username='user1', password='testas1')
    client.login(username='user1', password='testas1')
    resp = client.post('/locations/new/', {
        'name': 'Saldytuvas1',
        'description': '',
    })

    assert resp.status_code == 302, resp.context['form'].errors.get_json_data()
    assert resp.headers['location'] == '/'
    assert Location.objects.filter(name='Saldytuvas1').exists()


@pytest.mark.django_db
def test_sign_in_link(django_app: DjangoTestApp):
    resp: DjangoWebtestResponse = django_app.get('/sign/signin/')
    resp.click('Prisijungti')
    resp.click('Registruotis')


@pytest.mark.django_db
def test_sign_out(django_app: DjangoTestApp, user: User):
    resp: DjangoWebtestResponse = django_app.get('/')
    resp = resp.click('Atsijungti').follow().follow()
    resp.click('Prisijungti')


@pytest.mark.django_db
def test_remove_product_amount(django_app: DjangoTestApp, user: User):
    l1 = Location.objects.create(name='L1', user=user)
    product = Product.objects.create(name='pienas')
    product_item = ProductItem.objects.create(
        amount=4,
        product=product,
        location=l1,
    )
    resp: DjangoWebtestResponse = django_app.get(f'/products/{l1.id}')
    products = [form.instance for form in resp.context['products']]
    assert products == [product_item]
    resp.form['form-0-amount'] = 1
    resp = resp.form.submit().follow()
    assert resp.request.path == f'/products/{l1.id}'
    assert ProductItem.objects.get(id=product_item.id).amount == 3

