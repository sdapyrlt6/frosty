from django.urls import path
from frosty.authentication import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('signup/', views.signup, name="signup"),
    path('signin/', auth_views.LoginView.as_view(template_name='authentication/signin.html'), name='login'),
#    path('reset/', auth_views.PasswordResetView.as_view(template_name='authentication/reset.html'), name='password_reset'),
    path('signout/', views.signout, name="signout")

]