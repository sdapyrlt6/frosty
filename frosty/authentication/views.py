from django.contrib.auth import logout
from django.shortcuts import render, redirect


def signup(request):
    return render(request, "authentication/signup.html")


def signout(request):
    logout(request)
    return redirect("/")
