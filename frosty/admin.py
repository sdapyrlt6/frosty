from django.contrib import admin

# Register your models here.
from django.contrib import admin
from frosty.models import Shop, Location, Product, ProductItem


class ShopAdmin(admin.ModelAdmin):
    pass


admin.site.register(Shop, ShopAdmin)


class LocationAdmin(admin.ModelAdmin):
    pass


admin.site.register(Location, LocationAdmin)


class ProductAdmin(admin.ModelAdmin):
    pass


admin.site.register(Product, ProductAdmin)


class ProductItemAdmin(admin.ModelAdmin):
    pass


admin.site.register(ProductItem, ProductItemAdmin)
