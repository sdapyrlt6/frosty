from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from django.db import models


class Location(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False)
    description = models.TextField(max_length=500, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False)

    class Types(models.TextChoices):
        VEGETABLES = 'vegetables', _('Vegetables')
        FRUITS = 'fruits', _('Fruits')
        MEAT = 'meat', _('Meat products')
        MILK = 'milk', _('Milk products')
        OTHER = 'other', _('Other')

    type = models.CharField(
        max_length=50,
        choices=Types.choices,
        default=Types.OTHER,
    )

    def __str__(self):
        return self.name


class Shop(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False)

    def __str__(self):
        return self.name


class ProductItem(models.Model):
    amount = models.FloatField()
    expiration_date = models.DateField(null=True, blank=True)
    registration_date = models.DateField(auto_now_add=True)


    class Units(models.TextChoices):
        KILO = 'kg', _('Kilograms')
        LITRE = 'l', _('Litres')
        VNT = 'vnt', _('Unit')
    unit = models.CharField(
        max_length=5,
        choices=Units.choices,
        default=Units.VNT,
    )
    shop = models.ForeignKey(Shop, on_delete=models.SET_NULL, null=True, blank=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)

    def __str__(self):
        return self.product.name


