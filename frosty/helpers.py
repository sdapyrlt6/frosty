from frosty.forms import ProductForm
from frosty.models import Product
from frosty.models import Product
from frosty.models import Product


def get_product_by_name(form: ProductForm) -> Product:
    try:
        return Product.objects.get(
            name=form.cleaned_data['name'],
        )
    except Product.DoesNotExist:
        return form.save()