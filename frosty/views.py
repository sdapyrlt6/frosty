from django.contrib.auth.decorators import login_required
from django.db.models import F
from django.http import HttpRequest
from django.http import HttpResponse
from django.shortcuts import render, redirect

from frosty.forms import ProductItemForm, ProductItemFormSet
from frosty.forms import ProductForm
from frosty.helpers import get_product_by_name
from frosty.models import Location, ProductItem, Shop
from frosty.forms import LocationForm



@login_required
def location_list(request: HttpRequest) -> HttpResponse:
    return render(request, 'locations.html', {
        'locations': Location.objects.filter(user=request.user)
    })


@login_required
def new_product(request: HttpRequest, location_id: int) -> HttpResponse:
    if request.method == 'POST':
        product_form = ProductForm(request.POST)
        product_item_form = ProductItemForm(request.POST)
        if product_form.is_valid() and product_item_form.is_valid():
            product = get_product_by_name(product_form)
            product_item = product_item_form.save(commit=False)
            product_item.product = product
            product_item.location_id = location_id
            if product_item_form.cleaned_data['shop_name']:
                shop = Shop.objects.create(name=product_item_form.cleaned_data['shop_name'])
                product_item.shop = shop
            product_item.save()
            return redirect(f'/products/{location_id}')
    else:
        product_form = ProductForm()
        product_item_form = ProductItemForm()
    return render(request, 'products/new.html', {
        'product_form': product_form,
        'product_item_form': product_item_form,
        'location_id': location_id,
    })


@login_required
def new_location(request):
    if request.method == 'POST':
        form = LocationForm(request.POST)
        if form.is_valid():
            location = form.save(commit=False)
            location.user = request.user
            location.save()
            return redirect('/')
    else:
        form = LocationForm()
    return render(request, 'locations/locations_form.html', {
        'form': form
    })


@login_required
def product_list(request: HttpRequest, location_id: int) -> HttpResponse:
    if request.method == 'POST':
        formset = ProductItemFormSet(request.POST, queryset=(
            ProductItem.objects.
            select_related('product', 'shop').
            filter(location_id=location_id)
        ))
        if formset.is_valid():
            for product in formset.save(commit=False):
                product.amount = F('amount') - product.amount
                product.save()
            return redirect(f'/products/{location_id}')
    else:
        formset = ProductItemFormSet(queryset=(
            ProductItem.objects.
            select_related('product', 'shop').
            filter(location_id=location_id)
        ))
    return render(request, 'products/list.html', {
            'products': formset,
            'location_id': location_id,
        })



