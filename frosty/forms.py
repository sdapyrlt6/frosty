from django.core.exceptions import ValidationError
from django.forms import ModelForm, modelformset_factory
from django import forms
from frosty.models import Product
from frosty.models import ProductItem
from frosty.models import Location


class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = [
            'name',
            'type',
        ]


class ProductItemForm(ModelForm):
    class Meta:
        model = ProductItem
        fields = [
            'amount',
            'unit',
            'expiration_date',
            'shop',
        ]
    shop_name = forms.CharField(required=False)

    def clean(self):
        cleaned_data = super().clean()
        shop = cleaned_data.get("shop")
        shop_name = cleaned_data.get("shop_name")

        if shop and shop_name:
            self.add_error('shop_name', "taip negalima, nu nu nu!!! turi buti tik vienas parduotuve, arba pasirinkta, arna naujai irasyta")


class LocationForm(ModelForm):
    class Meta:
        model = Location
        fields = [
            'name',
            'description',
            ]


ProductItemFormSet = modelformset_factory(ProductItem, fields=('amount',), extra=0)
